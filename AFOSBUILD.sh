rm -rf /opt/ANDRAX/doona

cp -Rf $(pwd) /opt/ANDRAX/doona

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
